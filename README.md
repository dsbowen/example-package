# Example package

[![pipeline status](https://gitlab.com/dsbowen/example-package/badges/master/pipeline.svg)](https://gitlab.com/dsbowen/example-package/-/commits/master)
[![coverage report](https://gitlab.com/dsbowen/example-package/badges/master/coverage.svg)](https://gitlab.com/dsbowen/example-package/-/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fexample-package/HEAD)

This is an example package with template files for my preferred coding practices.

## Folder structure

- `docs/`
- `examples/` example notebooks and other files
- `src/` this is where the magic happens
- `tests/` unit tests
- `.gitignore`
- `.gitlab-ci.yml` CI/CD file
- `.gitpod.yml` configures gitpod prebuilt environment
- `LICENSE` MIT License
- `Makefile` defines a pipeline for autoformatting, unit testing, and code quality checks
- `MANIFEST.in` (optional) for including package data
- `README.md`
- `environment.yml` set python version and dependencies for conda environment
- `postBuild` runs script after binder build (by default installs an editable version of the package)
- `pyproject.toml` for release on PyPI
- `requirements.txt` used by venv, (possibly) conda environment, binder, gitpod, and CI services
- `runtime.txt` set python version, used by binder, gitpod, deployment services like heroku
- `setup.cfg` for release on PyPI
- `setup.py` enables editable installs
- `tox.ini` environment configuration file for automated unit testing

## Development environment

### Setup environment

With `venv`:

```bash
$ python -m venv venv
$ . venv/bin/activate # . venv/scripts/activate on windows
$ pip install -r requirements.txt
```

With `conda`:

```bash
$ conda create -n ENVIRONMENT_NAME -f environment.yml
$ conda activate ENVIRONMENT_NAME
$ pip install -r requirements.txt # if additional requirements are stored in requirements.txt that aren't in environment.yml
```

I prefer conda because it allows you to easily specify a python version. [virtualenv](https://virtualenv.pypa.io/en/latest/) also allows you to do this but I haven't tried it.

I recommend using `requirements.txt` even with conda because the `requirements.txt` is used by many other programs (binder, gitpod, heroku, CI services, etc.). I use `environment.yml` only to specify the python version upgrade pip.

To use the package in `src`, for example in your `examples` directory, install an editable version of your package with:

```bash
$ pip install -e .
```

### Add kernel

You'll want to add a kernel for your environment if working with `.ipynb` files.

```bash
$ pip install ipykernel # or conda install ipykernel
$ python -m ipykernel install --user --name ENVIRONMENT_NAME
```

### IDE

I've used jupyter lab and VS code and like them both. Both ship with anaconda. Currently I prefer VS code for `.py` files solely for the [python docstring generator](https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring) extension. As of 2021/03/18 jupyter lab appears to have to equivalent. 

However, jupyter has better notebook extensions, and I plan to switch to it if someone creates a python docstring generator extension.

I'm also curious to try VS IDE, which is apparently a full-featured IDE version of VS code.

## Pipeline

This example package implements a pipeline which runs autoformatting, unit tests, linting, and doc creation and testing. Run the pipeline with:

```bash
$ make pipeline
```

This generates reports (e.g., about code quality) in `reports/` and docs in `docs/_build`.

The pipeline is modular. Below, I provide `make` commands for running individual nodes of the pipeline.

`.gitlab-ci.yml` implements a similar CI pipeline which additionally checks the build. However, it doesn't autoformat (it seems gitlab-CI doesn't let you change code files).

Modify `Makefile` to tailor the pipeline. `.gitlab-ci.yml` calls `make` commands, so any changes to the makefile will also be made to your CI pipeline. Don't forget to add third-party packages to `requirements.txt` if you need them in the pipeline (e.g., `pytest` for unit testing).

### Format

Run just the formatting node of the pipeline with:

```bash
$ make format
```

`black` is beautiful, but `yapf` seems great too.

### Test

Run unit tests and generate a coverage report with `make test`. Run `make testserve` and go to [http://localhost:8000/] to see the coverage report (for the last tox environment). Open `reports/test.txt` to view the unit test report. (Run `tox` to run unit tests without the coverage report).

Note that my version of `.gitlab-ci.yml` only tests against python3.9. To test against other environments, create a new job in `.gitlab-ci.yml` that looks something like this:

```yml
test-py38-job:
    extends: test-job
    image: python:3.8
    variables:
        - TOXENV: py38
```

### Lint

Run linting with `make lint`. Open `reports/lint.txt` to view the code quality report.

I currently use `pylint`. It's very strict.

### Typehint

Run type-hinting with `make typehint`. Open `reports/typehint.txt` to view the type-hinting report.

I currently use `mypy`. The one thing I don't like about it is that it only checks type hints in functions which themselves are annotated. e.g., say we've got:

```python
def f(name: str) -> str:
    return f"Hello, {name}!"
```

`mypy` will catch the following:

```python
def g(x: str) -> str:
    return f(123)
```

but not:

```python
def g(x):
    return f(123)
```

### Docs

Run `make docs` to both create and test your docs. Run `make docserve` to serve your docs on [http://localhost:8000/](http://localhost:8000/). Open `reports/output.txt` to view the doctest report.

I used to use [MkDocs](https://www.mkdocs.org/) but recently upgraded to [Sphinx](https://www.sphinx-doc.org/en/master/). The autodocs are outstanding.

Tips:

1. I use Sphinx's [setuptools integration](https://www.sphinx-doc.org/en/master/usage/advanced/setuptools.html) to keep all the configuration variables in one place (`setup.cfg`).
2. After using Sphinx's `autogen`, go into the stub `.rst` files and add the `:members:` option to the `automodules`. This tells Sphinx autodoc to automatically document all the functions and classes from your python modules.
3. Put [doctests](https://www.sphinx-doc.org/en/master/usage/extensions/doctest.html) in your docstrings. `make docs` automatically runs Sphinx's doctests to check the example code in your docstrings against your code's actual behavior.

I think of doctests as additional unit tests. Accordingly, I built the `.gitlab-ci.yml` so that your pipeline will fail if your doctests fail.

## In-browser dev environments

Set up a [binder](https://mybinder.org/) for a sandbox in-browser environment. This is the perfect solution for showcasing examples or allowing data scientists to reproduce your analysis without setting up their own environment. Opening jupyter lab instead of jupyter notebook is as simple as adding `?urlpath=lab` to the notebook URL binder gives you by default. [See here](https://mybinder.readthedocs.io/en/latest/howto/user_interface.html#jupyterlab) for specific instructions on converting notebook URLs to lab URLs.

Binder has at least two limitations:

1. Although it can serve simple web apps, I wasn't able to make it work for more advanced apps (my best guess is it doesn't work when your app needs a database)
2. It isn't a full-featured cloud-based IDE

[Gitpod](https://gitpod.io/) is a great cloud-based IDE if a lightweight binder doesn't cut it. I haven't tried promising alternatives such as [Codeanywhere](https://codeanywhere.com/) and [Red Hat CodeReady](https://developers.redhat.com/products/codeready-workspaces/overview). Edit `.gitpod.yml` to customize your prebuilt workspace.

Tip:

1. One downside of gitpod is that you can't work with notebooks in its IDE. Fortunately, there's an easy workaround. Install jupyter in the workspace and open jupyter from the terminal with `jupyter lab --NotebookApp.allow_origin=\'$(gp url 8888)\'`.

## PyPI

This example package includes template files (specifically `setup.cfg`, `pyproject.toml`, and `LICENSE`) so you can easily release your package on PyPI. Tutorial [here](https://packaging.python.org/tutorials/packaging-projects/).

Two tips:

1. Don't forget to add the package requirements to `install_requires` in `setup.cfg`
2. If your package needs non-`.py` files, include paths to those in a file in your root directory named `MANIFEST.in`.

## Badges

Enable at least build (pipeline) passing and code coverage badges. Gitlab gives you these when you set up [gitlab-CI](https://docs.gitlab.com/ee/ci/pipelines/settings.html#pipeline-badges). Others that I like include:

- [black coding style](https://github.com/psf/black#show-your-style)
- [PyPI version](https://badge.fury.io/) if you release it on PyPI
- Binder or gitpod badge
