from example_package.greet import Greet

def test_greet():
    assert Greet('Dillon').make_greeting('Sarah') == 'Dillon says, "Hello, Sarah!"'

def test_meet():
    assert Greet('Dillon').make_meeting('John', '1:00 PM') == 'Dillon meets John at 1:00 PM'
