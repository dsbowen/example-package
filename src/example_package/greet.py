"""Give someone a simple greeting.
"""

from __future__ import annotations

from typing import Union

Time = Union[str, int]


class Greet:
    """This class creates a greeting.

    Attributes:
        name (str): Your name
    """

    def __init__(self, name: str):
        self.name = name

    def make_greeting(self, person: str) -> str:
        """Create greeting

        Args:
            person (str): Person to greet

        Returns:
            str: Greeting

        Examples:
            .. testsetup::

                from example_package.greet import Greet

            .. doctest::

                >>> Greet('Dillon').make_greeting('Sarah')
                'Dillon says, "Hello, Sarah!"'
        """
        return f'{self.name} says, "Hello, {person}!"'

    def make_meeting(self, person: str, time: Time = "noon") -> str:
        """Create meeting

        Args:
            person (str): Person to meet
            time (Time, optional): Time to meet the person. Defaults to 'noon'.

        Returns:
            str: Meeting

        Examples:
            .. testsetup::

                from example_package.greet import Greet

            .. doctest::

                >>> Greet('Dillon').make_meeting('Sarah')
                'Dillon meets Sarah at noon'
        """
        return f"{self.name} meets {person} at {time}"
